package com.tm.blog.services.controller;

import com.tm.blog.services.repository.UserRepository;
import com.tm.blog.services.service.CommentsService;
import com.tm.blog.services.service.PostsService;
import com.tm.blog.services.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

/**
 * Test CommController
 */
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource("/application-test.properties")
@WithUserDetails("admin")
class CommControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private PostController postController;

    @Autowired
    private PostsService postsService;

    @Autowired
    private CommentsService commentsService;

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    /**
     * The test of creation is Comment
     */
    @Test
    void create() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.post("/posts")
                        .param("title", "TestTitle")
                        .param("content", "ContentTest"))
                        .andExpect(MockMvcResultMatchers.redirectedUrl("/posts"));

        Long actual = postsService.findTopByOrderByIdDesc().getId();

        this.mockMvc.perform(MockMvcRequestBuilders.post("/posts/{id}/comments",actual)
                .param("commentBody", "Test Comment"))
                .andExpect(MockMvcResultMatchers.redirectedUrl("/posts/" + actual));

        commentsService.delete(actual + 1);
        postsService.delete(actual);
    }
}