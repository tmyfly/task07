package com.tm.blog.services.controller;

import com.tm.blog.services.repository.UserRepository;
import com.tm.blog.services.service.PostsService;
import com.tm.blog.services.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test PostController
 */
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource("/application-test.properties")
@WithUserDetails("admin")
public class PostControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private PostController postController;

    @Autowired
    private PostsService postsService;

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    /**
     * postsMain page accessibility test - Post Page
     */
    @Test
    public void postsMain() throws Exception{
        this.mockMvc.perform( get("/posts"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(status().isOk());
    }

    /**
     * postsCreatePage  page accessibility test - Post Create Page
     */
    @Test
    public void postsCreatePage() throws Exception{
        this.mockMvc.perform( get("/posts/create"))
                .andDo(print())
                .andExpect(authenticated())
                .andExpect(status().isOk());
    }

    /**
     * postsCreate The test of creation is Post
     */
    @Test
    public void postsCreate() throws Exception{
        this.mockMvc.perform(MockMvcRequestBuilders.post("/posts")
                .param("title", "TestTitle")
                .param("content", "ContentTest"))
                .andExpect(MockMvcResultMatchers.redirectedUrl("/posts"));

        Long actual = postsService.findTopByOrderByIdDesc().getId();
        postsService.delete(actual);
    }
}