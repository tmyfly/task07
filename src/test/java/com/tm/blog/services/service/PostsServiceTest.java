package com.tm.blog.services.service;

import com.tm.blog.services.dao.Posts;
import com.tm.blog.services.repository.PostsRepository;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

/**
 * Posts service test
 */
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
class PostsServiceTest {
    @Autowired
    private PostsService postsService;

    @Mock
    private PostsRepository postsRepository;

    /**
     *  The test of creation is Posts
     */
    @Test
    void create() {
        Posts posts = new Posts();
        posts.setTitle("Test1");
        posts.setContent("___Test___");

        postsService.create(posts);
        Posts actual = postsService.findTopByOrderByIdDesc();

        Assert.assertEquals("Test1", actual.getTitle());
        postsService.delete(actual.getId());
    }

    /**
     *  The test of update is Posts
     */
    @Test
    void update() {
        Posts posts = new Posts();
        posts.setTitle("Test1");
        posts.setContent("___Test___");
        postsService.create(posts);

        Posts posts1 = new Posts();
        posts1.setId(postsService.findTopByOrderByIdDesc().getId());
        posts1.setTitle("Test2");
        posts1.setContent("___Test___");

        postsService.update(posts1);

        Posts actual = postsService.readById(postsService.findTopByOrderByIdDesc().getId());
        Assert.assertEquals("Test2", actual.getTitle());

        Long actualId = postsService.findTopByOrderByIdDesc().getId();
        postsService.delete(actualId);
    }

    /**
     *  The test of delete is Posts
     */
    @Test
    void delete() throws NullPointerException{
        Posts posts = new Posts();
        posts.setTitle("Test1");
        posts.setContent("___Test___");

        postsService.create(posts);

        Long actual = postsService.findTopByOrderByIdDesc().getId();
        postsService.delete(actual);

        Throwable thrown = catchThrowable(() -> {
            postsService.readById(actual);
        });
        assertThat(thrown).isInstanceOf(NullPointerException.class);
        assertThat(thrown.getMessage()).isNotBlank();
    }
}