package com.tm.blog.services.service;

import com.tm.blog.services.dao.Role;
import com.tm.blog.services.dao.User;
import com.tm.blog.services.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.Assert;

import java.util.Collections;

@SpringBootTest
public class UserDetailsTest {
    @MockBean
    private UserRepository userRepository;
    @Autowired
    private UserService userService;

    @Test
    public void testUser(){
        final var user = new User();
        user.setId(1l);
        user.setUsername("User1");
        user.setPassword("123");
        user.setPasswordConfirm("123");
        user.setRoles(Collections.singleton(new Role(1L, "ROLE_USER")));

        Mockito.when(userRepository.findByUsername("User1")).thenReturn(user);

        final UserDetails userDetails = userService.loadUserByUsername("User1");
        Assert.isTrue("User1".equals(userDetails.getUsername()), "Should be 'User1'");

    }
}
