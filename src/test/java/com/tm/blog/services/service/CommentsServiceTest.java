package com.tm.blog.services.service;

import com.tm.blog.services.dao.Comments;
import com.tm.blog.services.dao.Posts;
import com.tm.blog.services.repository.CommentsRepository;
import com.tm.blog.services.repository.PostsRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.junit.Assert.*;

/**
 * Comments service test
 */
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
public class CommentsServiceTest {

    @Autowired
    private CommentsService commentsService;

    @Autowired
    PostsService postsService;

    @Mock
    private CommentsRepository commentsRepository;
    @Mock
    private PostsRepository postsRepository;


    /**
     * The test of creation is Comments
     */
    @Test
    public void create() {
        Posts posts = new Posts();
        posts.setTitle("Test1");
        posts.setContent("___Test___");

        postsService.create(posts);
        Posts postsTmp = postsService.findTopByOrderByIdDesc();

        Comments comments = new Comments();
        comments.setId(postsTmp.getId()+1);
        comments.setCommentBody("CommTest");
        comments.setPostId(postsTmp.getId());

        commentsService.create(comments);

        Comments actual = commentsService.findTopByOrderByIdDesc();
        Assert.assertEquals("CommTest", actual.getCommentBody());

        postsService.delete(postsTmp.getId());
    }

    /**
     * The test of update is Comments
     */
    @Test
    public void update() {
        Posts posts = new Posts();
        posts.setTitle("Test1");
        posts.setContent("___Test___");

        postsService.create(posts);
        Posts postsTmp = postsService.findTopByOrderByIdDesc();

        Comments comments = new Comments();
        comments.setId(postsTmp.getId()+1);
        comments.setCommentBody("CommTest");
        comments.setPostId(postsTmp.getId());

        commentsService.create(comments);

        Comments comments2 = new Comments();
        comments2.setId(commentsService.findTopByOrderByIdDesc().getId());
        comments2.setCommentBody("CommTest2");
        comments2.setPostId(comments.getPostId());

        commentsService.update(comments2);

        Comments actual = commentsService.readById(commentsService.findTopByOrderByIdDesc().getId());
        Assert.assertEquals("CommTest2", actual.getCommentBody());

        postsService.delete(postsTmp.getId());
    }

    /**
     * The test of delete is Comments
     */
    @Test
    public void delete() {

        Posts posts = new Posts();
        posts.setTitle("Test1");
        posts.setContent("___Test___");

        postsService.create(posts);
        Posts postsTmp = postsService.findTopByOrderByIdDesc();


        Comments comments = new Comments();
        comments.setId(postsTmp.getId()+1);
        comments.setCommentBody("CommTest");
        comments.setPostId(postsTmp.getId());

        commentsService.create(comments);

        Long actual = commentsService.findTopByOrderByIdDesc().getId();
        commentsService.delete(actual);

        Throwable thrown = catchThrowable(() -> {
            commentsService.readById(actual);
        });
        assertThat(thrown).isInstanceOf(NullPointerException.class);
        assertThat(thrown.getMessage()).isNotBlank();

        postsService.delete(postsTmp.getId());
    }
}