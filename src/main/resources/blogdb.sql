CREATE SEQUENCE hibernate_sequence START 1;

CREATE TABLE posts(
    id serial PRIMARY KEY,
    title VARCHAR (255) NOT NULL,
    content VARCHAR (1000) NOT NULL
);

CREATE TABLE comments (
    id serial PRIMARY KEY,
    comment_body VARCHAR (1000) NOT NULL,
    post_id INT,
    FOREIGN KEY (post_id) REFERENCES posts (id)
);
