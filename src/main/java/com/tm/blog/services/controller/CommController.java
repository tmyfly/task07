package com.tm.blog.services.controller;

import com.tm.blog.services.dao.Comments;
import com.tm.blog.services.dao.Posts;
import com.tm.blog.services.service.CommentsService;
import com.tm.blog.services.service.PostsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/posts/{posts_id}/comments")
public class CommController {

    private final CommentsService commentsService;
    private final PostsService postsService;

        public CommController(CommentsService commentsService, PostsService postsService) {
        this.commentsService = commentsService;
        this.postsService = postsService;
    }

    @GetMapping("/create")
    public String createComments(@PathVariable Long posts_id, Model model) {
        model.addAttribute("comments", new Comments());
        model.addAttribute("postIdT", posts_id);
        model.addAttribute("postId", postsService.readById(posts_id));
        return "commcreate";
    }

    @PostMapping()
    public String create(@PathVariable Long posts_id, @ModelAttribute("comments") Comments comments) {
        comments.setPostId(posts_id);
        commentsService.create(comments);
        return "redirect:/posts/" + posts_id;
    }

    @GetMapping("/{id}")
    public String readById(@PathVariable Long id, Model model) {
        model.addAttribute("comments", commentsService.readById(id));
        return "commviev";
    }

    @GetMapping("/edit/{id}")
    public String getEditForm(@PathVariable Long posts_id, @PathVariable Long id, Model model) {
        model.addAttribute("postIdT", posts_id);
        model.addAttribute("comments", commentsService.readById(id));
        return "commedit";
    }

    @PostMapping("/update")
    public String update(@PathVariable Long posts_id, @ModelAttribute Comments comments) {
        commentsService.update(comments);
        return "redirect:/posts/{posts_id}";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id) {
        commentsService.delete(id);
        return "redirect:/posts/{posts_id}";
    }
}
