package com.tm.blog.services.controller;

import com.tm.blog.services.dao.Posts;
import com.tm.blog.services.service.CommentsService;
import com.tm.blog.services.service.PostsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping({"/posts"})
public class PostController {
    private final int ROW_PER_PAGE = 5;

    private final PostsService postsService;
    private final CommentsService commentsService;


    public PostController(PostsService postsService, CommentsService commentsService) {
        this.postsService = postsService;
        this.commentsService = commentsService;
    }

    @GetMapping
    public String getPosts(Model model, @RequestParam(value = "page", defaultValue = "1") int pageNumber) {
        final List<Posts> posts = postsService.findAll(pageNumber, ROW_PER_PAGE);
        final long count = postsService.count();
        final boolean hasPrev = pageNumber > 1;
        final boolean hasNext = (pageNumber * ROW_PER_PAGE) < count;

        model.addAttribute("posts", posts);
        model.addAttribute("hasPrev", hasPrev);
        model.addAttribute("prev", pageNumber - 1);
        model.addAttribute("hasNext", hasNext);
        model.addAttribute("next", pageNumber + 1);
        return "postspage";
    }

    @GetMapping("/create")
    public String createPost() {
        return "postcreate";
    }

    @PostMapping
    public String create(@ModelAttribute Posts posts) {
        postsService.create(posts);
        return "redirect:/posts";
    }

    @GetMapping("/{id}")
    public String readById(@PathVariable Long id, Model model) {
        model.addAttribute("posts", postsService.readById(id));
        model.addAttribute("commentsPostId", commentsService.getByComment(id));
        return "postsviev";
    }

    @GetMapping("/edit/{id}")
    public String getEditForm(@PathVariable Long id, Model model) {
        model.addAttribute("posts", postsService.readById(id));
        return "postedit";
    }

    @PostMapping("/update")
    public String update(@ModelAttribute Posts posts) {
        postsService.update(posts);
        return "redirect:/posts";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id) {
        postsService.delete(id);
        return "redirect:/posts";
    }
}
