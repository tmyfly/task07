package com.tm.blog.services.dto;

import com.tm.blog.services.dao.Posts;

import javax.persistence.*;

public class Comments {
    private Long id;
    private String commentBody;
    private Long postId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCommentBody() {
        return commentBody;
    }

    public void setCommentBody(String commentBody) {
        this.commentBody = commentBody;
    }

    public Long getPostId() {
        return postId;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }

    @Override
    public String toString() {
        return "Comments{" +
                "id=" + id +
                ", commentBody='" + commentBody + '\'' +
                ", postId=" + postId +
                '}';
    }
}
