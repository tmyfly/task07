package com.tm.blog.services.service;

import com.tm.blog.services.dao.Comments;

import java.util.List;

public interface CommentsService {
    Comments create(Comments comments);
    Comments readById(Long id);
    Comments update(Comments comments);
    void delete(Long id);

    List<Comments> getByComment(Long postId);
    Comments findTopByOrderByIdDesc();
}
