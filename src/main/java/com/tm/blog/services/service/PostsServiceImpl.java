package com.tm.blog.services.service;

import com.tm.blog.services.dao.Posts;
import com.tm.blog.services.repository.PostsRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PostsServiceImpl implements PostsService{

    private final PostsRepository postsRepository;

    public PostsServiceImpl(PostsRepository postsRepository) {
        this.postsRepository = postsRepository;
    }

    @Override
    public Posts create(Posts posts) {
        try {
            return postsRepository.save(posts);
        }catch (RuntimeException e){
            throw new NullPointerException("Posts Null");
        }
    }

    @Override
    public Posts readById(Long id) {
        final Optional<Posts> optional = postsRepository.findById(id);
        if(optional.isPresent()){
            return optional.get();
        }
        throw new NullPointerException("Posts Null");
    }

    @Override
    public Posts update(Posts posts) {
        final Posts oldPost = readById(posts.getId());
        if (posts != null && oldPost != null) {
            posts.setCommentsList(oldPost.getCommentsList());
            return postsRepository.save(posts);
        }
        throw new NullPointerException("update error");
    }

    @Override
    public void delete(Long id) {
        final Posts posts = readById(id);
        if (posts != null) {
            postsRepository.delete(posts);
        } else {
            throw new NullPointerException("delete error");
        }
    }

    @Override
    public List<Posts> findAll(int pageNumber, int rowPerPage) {
        final List<Posts> posts = new ArrayList<>();
        final Pageable sortedByLastUpdateDesc = PageRequest.of(pageNumber - 1, rowPerPage,
                Sort.by("id").ascending());
        postsRepository.findAll(sortedByLastUpdateDesc).forEach(posts::add);
        return posts;
    }

    @Override
    public Long count() {
        return postsRepository.count();
    }

    @Override
    public Posts findTopByOrderByIdDesc() {
        return postsRepository.findTopByOrderByIdDesc();
    }
}
