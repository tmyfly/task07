package com.tm.blog.services.service;

import com.tm.blog.services.dao.Posts;

import java.util.List;

public interface PostsService {
    Posts create(Posts posts);
    Posts readById(Long id);
    Posts update(Posts posts);
    void delete(Long id);

    List<Posts> findAll(int pageNumber, int rowPerPage);
    Long count();

    Posts findTopByOrderByIdDesc();
}
