package com.tm.blog.services.service;

import com.tm.blog.services.dao.Comments;
import com.tm.blog.services.repository.CommentsRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CommentsServiceImpl implements CommentsService{

    private final CommentsRepository commentsRepository;
    public CommentsServiceImpl(CommentsRepository commentsRepository) {
        this.commentsRepository = commentsRepository;
    }

    @Override
    public Comments create(Comments comments) {
        try {
            return commentsRepository.save(comments);
        }catch (RuntimeException e){
            throw new NullPointerException("Comments Null");
        }
    }

    @Override
    public Comments readById(Long id) {
        final Optional<Comments> optional = commentsRepository.findById(id);
        if(optional.isPresent()){
            return optional.get();
        }
        throw new NullPointerException("Comments Null");
    }

    @Override
    public Comments update(Comments comments) {
        final Comments oldComments = readById(comments.getId());
        if (comments != null && oldComments != null) {
            return commentsRepository.save(comments);
        }
        throw new NullPointerException("update error");
    }

    @Override
    public void delete(Long id) {
        final Comments comments = readById(id);
        if (comments != null) {
            commentsRepository.delete(comments);
        } else {
            throw new NullPointerException("delete error");
        }
    }

    @Override
    public List<Comments> getByComment(Long postId) {
        final List<Comments> commentsListPostId = commentsRepository.getByComment(postId);
        return commentsListPostId.isEmpty() ? new ArrayList<>() : commentsListPostId;
    }

    @Override
    public Comments findTopByOrderByIdDesc() {
        return commentsRepository.findTopByOrderByIdDesc();
    }
}
