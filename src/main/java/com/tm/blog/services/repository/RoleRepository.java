package com.tm.blog.services.repository;

import com.tm.blog.services.dao.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
