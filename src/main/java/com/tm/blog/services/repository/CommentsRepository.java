package com.tm.blog.services.repository;

import com.tm.blog.services.dao.Comments;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentsRepository extends JpaRepository<Comments, Long> {
    @Query(value = "select * from comments where post_id = ?1", nativeQuery=true)
    List<Comments> getByComment(Long postId);

    Comments findTopByOrderByIdDesc();
}